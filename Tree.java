//package treesLabSheet;

//import generaltree.*;
import java.util.*;

/**
 *
 * @author ajb
 */
public class Tree {
    TreeNode root;
    int height;
    int numNodes;
    public Tree(){
        root=null;
    }

    public void toString2() { //BFS
        Queue<TreeNode> toVisit = new LinkedList<>();
        toVisit.add(root);
        while(toVisit.isEmpty() == false) {
            for (TreeNode tn : toVisit.peek().offspring) {
                if (tn != null) {
                    toVisit.add(tn);
                }
            }
                System.out.println(toVisit.remove());
        }
    }

    /*public static boolean hasUnvisitedOffspring(TreeNode node) {
        for (TreeNode t : node.offspring) {
            if (t == null) {
                continue;
            }
            else if (t instanceof TreeNode) {
                TreeNode treenode = (TreeNode) t;
                if (! t.visited) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void addFirstUnvisitedToStack(TreeNode topItem, Stack treeNodeStack) {
        for (TreeNode o : topItem.offspring) {
            if (o == null) {
             continue;
            }
            if (!o.visited) {
                o.visited = true;
                System.out.println(o);
                treeNodeStack.push(o);
                break;
            }
        }
    }*/

    public void toString3() { //DFS
        Stack<TreeNode> path = new Stack<>();
        path.push(root);

        System.out.println(path.peek());
        while(!path.isEmpty()) {
            TreeNode t = path.peek();

            for (TreeNode tn :t.offspring) {
                if (tn == null) {
                    //System.out.println("No Child Pop " + path.pop());
                    path.pop();
                    break;
                }
                else if (tn.visited == false) {
                    //System.out.println("    Push " + path.push(tn));
                    System.out.println(path.push(tn));
                    tn.visited = true;
                    break;
                }
            }
        }

/*        Stack<TreeNode> treeNodeStack = new Stack<>();
        treeNodeStack.push(t.root);
        System.out.println(treeNodeStack.peek()); //Print root node

        while(!treeNodeStack.isEmpty()) {
            TreeNode topItem = treeNodeStack.peek();

            if(hasUnvisitedOffspring(topItem)) {
                addFirstUnvisitedToStack(topItem,treeNodeStack);
            }
            else {
                treeNodeStack.pop();
            }
        }*/

    }

    public static Tree createExampleTree1(){
        Tree t= new Tree();
        t.root=new TreeNode("ARSENAL");
        t.root.add(new TreeNode("Forty"));
        t.root.add(new TreeNode("Nine"));
        t.root.add(new TreeNode("Undefeated"));
        t.root.offspring[0].add(new TreeNode("I"));
        t.root.offspring[0].add(new TreeNode("Bet"));
        t.root.offspring[1].add(new TreeNode("You"));
        t.root.offspring[2].add(new TreeNode("Are"));
        t.root.offspring[2].add(new TreeNode("Sick"));
        t.root.offspring[2].add(new TreeNode("Of"));
        t.root.offspring[2].add(new TreeNode("Arsenal"));
        t.root.offspring[2].offspring[1].add(new TreeNode("Examples"));

        return t;
    }

    public static Tree createExampleTree2(){
        Tree t= new Tree();
        t.root=new TreeNode("Trees:");
        t.root.add(new TreeNode("Oak"));
        t.root.add(new TreeNode("Willow"));
        t.root.offspring[0].add(new TreeNode("Beech"));
        t.root.offspring[0].add(new TreeNode("Horse Chestnut"));

        return t;
    }
    public static Tree buildExample(){
        Tree t =new Tree();
        t.root=new TreeNode("Hello");
        t.root.add(new TreeNode("what"));
        t.root.offspring[0].add(new TreeNode("write"));
        t.root.offspring[0].offspring[0].add(new TreeNode("BAH"));
        t.root.add(new TreeNode("shall"));
        t.root.offspring[1].add(new TreeNode("God"));
        
        Stack stack = new Stack();
        stack.push("LALALALALA");
        Object s=stack.pop();
        Queue q = new LinkedList();
        return t;
        
        
        
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Tree t = Tree.createExampleTree1();
        //t.toString2();
        System.out.println(" ");
        t.toString3(); //print Arsenal Forty I Bet Nine You Undefeated Are Sick Examples of Arsenal
    }
    
}
